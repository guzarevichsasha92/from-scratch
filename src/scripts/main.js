import "../styles/main.css";
import bootImage from "./../../assets/images/boot.png";
import arrow from "./../../assets/icons/arrow.png";
import { createElement } from "./createElement";

const main = createElement({ el: "main", className: "main" });
const mainWrapper = createElement({
  el: "div",
  parent: main,
  className: "main__wrapper",
});
const mainLeftSlider = createElement({
  el: "div",
  parent: mainWrapper,
  className: "main__slider",
});
mainLeftSlider.classList.add("main__left-slider");
const mainLeftArrow = createElement({
  el: "img",
  parent: mainLeftSlider,
  className: "main__left-arrow",
});
mainLeftArrow.setAttribute("src", arrow);
mainLeftArrow.classList.add("main__arrow");
const mainHeroBlock = createElement({
  el: "div",
  parent: mainWrapper,
  className: "main__hero-block",
});
const mainHeroTitle = createElement({
  el: "p",
  parent: mainHeroBlock,
  className: "main__hero-title",
});
const mainHeroImage = createElement({
  el: "img",
  parent: mainHeroBlock,
  className: "main__hero-image",
});
mainHeroImage.setAttribute("src", bootImage);
// mainHeroImage.classList.add("shown")

const mainRightSlider = createElement({
  el: "div",
  parent: mainWrapper,
  className: "main__slider",
});
mainRightSlider.classList.add("main__right-slider");
const mainRightArrow = createElement({
  el: "img",
  parent: mainRightSlider,
  className: "main__right-arrow",
});
mainRightArrow.setAttribute("src", arrow);
mainRightArrow.classList.add("main__arrow");

export { main, mainRightSlider, mainLeftSlider, mainHeroImage };
