import "./../styles/main-footer.css";
import { createElement } from "./createElement";
import { main } from "./main";
import filledStar from "./../../assets/images/filled-star.png";
import unfilledStar from "./../../assets/images/unfilled-star.png";

const mainFooter = createElement({
  el: "footer",
  parent: main,
  className: "main-footer",
});

const itemInfo = createElement({
  el: "div",
  parent: mainFooter,
  className: "main-footer__item-info",
});

const cta = createElement({
  el: "button",
  parent: mainFooter,
  className: "main-footer__cta",
});
cta.innerText = "BUY NOW";

const price = createElement({
  el: "p",
  parent: itemInfo,
  className: "main-footer__item-info__price",
});
price.innerHTML = "&#x24;749";
const stars = createElement({
  el: "ul",
  parent: itemInfo,
  className: "main-footer__item-info__rating",
});
for (let starCounter = 0; starCounter < 5; starCounter++) {
  const star = createElement({ el: "li", parent: stars });
  const starImage = createElement({
    el: "img",
    parent: star,
    className: "main-footer__item-info__rating_star",
  });
  if (starCounter < 4) {
    starImage.setAttribute("src", filledStar);
  } else {
    starImage.setAttribute("src", unfilledStar);
  }
}

const itemDescription = createElement({
  el: "div",
  parent: itemInfo,
  className: "main-footer__item-info__description",
});
const paragraph1 = createElement({ el: "p", parent: itemDescription });
paragraph1.innerText = "A Woocommerce product gallery Slider for Slider";
const paragraph2 = createElement({ el: "p", parent: itemDescription });
paragraph2.innerText = "Revolution with mind-blowing visuals.";
