import "./../styles/style.css";
import "./header";
import "./main";
import "./main-footer";
import { main } from "./main";
import { header } from "./header";

const app = document.getElementById("app");

app.appendChild(header);
app.appendChild(main);
