import "../styles/header.css";
import { createElement } from "./createElement";
import twitterIconImage from "../../assets/icons/twitter-icon.svg";
import facebookIconImage from "../../assets/icons/facebook-icon.svg";
import instagramIconImage from "../../assets/icons/instagram-icon.svg";
import logoImage from "../../assets/icons/logo.svg";
const header = createElement({
  el: "header",
  className: "header",
});

const logo = createElement({
  el: "img",
  parent: header,
});
logo.setAttribute("src", logoImage);

const headerSliderName = createElement({
  el: "p",
  parent: header,
  className: "header__slider-name",
});
headerSliderName.innerText = "WOOCOMMERCE  PRODUCT  SLIDER";

const headerSocial = createElement({
  el: "div",
  parent: header,
  className: "header__social",
});
const twitterIcon = createElement({
  el: "img",
  parent: headerSocial,
});
twitterIcon.setAttribute("src", twitterIconImage);
const facebookIcon = createElement({
  el: "img",
  parent: headerSocial,
});
facebookIcon.setAttribute("src", facebookIconImage);
const instagramIcon = createElement({
  el: "img",
  parent: headerSocial,
});
instagramIcon.setAttribute("src", instagramIconImage);

const headerCart = createElement({
  el: "p",
  parent: header,
});

headerCart.innerHTML = "Cart&#40;0&#41;";

export { header };
