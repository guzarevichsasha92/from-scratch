import { createElement } from "./createElement";
import image1 from "./../../assets/images/boot.png";
import image2 from "./../../assets/images/boot2.png";
import image3 from "./../../assets/images/boot3.png";
import image4 from "./../../assets/images/boot4.png";
import { mainRightSlider, mainLeftSlider, mainHeroImage } from "./main";

const images = [image1, image2, image3, image4];
let imageCounter = 0;

mainRightSlider.addEventListener("click", () => {
  // mainHeroImage.style.right = window.innerWidth.toString() + "px"
  imageCounter = (imageCounter + 1) % images.length;
  mainHeroImage.setAttribute("src", images[imageCounter]);
});
mainLeftSlider.addEventListener("click", () => {
  // mainHeroImage.style.right = window.innerWidth.toString() + "px"
  imageCounter = Math.abs(imageCounter - 1) % images.length;
  mainHeroImage.setAttribute("src", images[imageCounter]);
});
